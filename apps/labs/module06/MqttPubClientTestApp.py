from labs.module06 import MqttClientConnector
from labs.common import SensorData
from labs.common import DataUtil
import logging

'''This class is the mqtt client that publishes messages to a topic.
   It generates a simple SensorData object that can be converted to a JSON string,
   and publish the string by using MqttClientConnector.
'''
class MqttPubClientTestApp():
    logging.basicConfig(level=logging.INFO)#set the log to display level info
    
    '''Generate a simple SensorData object and initialize its parameters;
       Convert the SensorData object to a JSON string.
       This is used to test.
    '''
    def generateMessage(self):
        sensorData = SensorData.SensorData()
        dataUtil = DataUtil.DataUtil()
        sensorData.curValue = 20
        sensorData.minValue = 3
        sensorData.maxValue = 28
        sensorData.avgValue = 17
        sensorData.sampleCount = 6
        jsondata = dataUtil.toJsonFromSensorData(sensorData)#converting SensorData object to JSON string through DataUtil.toJsonFromSensorData()
        logging.info("Publish JSON string data:")
        logging.info(jsondata)#show JSON string
        return jsondata
    
    '''Connect to the mqtt broker, and publish a test message to the given topic
    '''
    def run(self):
        mqttClient = MqttClientConnector.MqttClientConnector()
        mqttClient.connect()
        topic = 'mqtt-test'
        payload = self.generateMessage()
        qos = 2
        mqttClient.publishMessage(topic, str(payload), qos)
        #mqttClient.disconnect()
        
'''The main function
'''        
if __name__ == '__main__':
    mqttpub = MqttPubClientTestApp()
    mqttpub.run()
        
    
    
