import paho.mqtt.client as mqtt
from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.common import DataUtil
import time
import logging

'''This class implements MqttCallback working as a connector to realize mqtt protocol.
   It contains the methods which can connect and disconnect to a remote host, 
   publish and subscribe messages, as well as convert the message to JSON or an object.
'''
class MqttClientConnector():
    default_protocol = "tcp"
    logging.basicConfig(level=logging.INFO)#set the log to display level info
    
    '''The constructor loads the configuration file and sets the value of host, port and client id
    '''
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('')
        self.config.loadConfig()
        self.host = self.config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.HOST_KEY)
        self.port = int(self.config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.PORT_KEY))
        self.client_id = time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))#get the current time as client id
        self.client = mqtt.Client(self.client_id)
        logging.info("Using client ID for broker conn: " + self.client_id)
        self.brokerAddr = self.default_protocol + "://" + self.host + ":" + str(self.port)
        logging.info("Using URL for broker conn: " + self.brokerAddr)
    
    '''Connect to the remote mqtt broker
    '''    
    def connect(self):
        self.client.connect(self.host, self.port, 600)
        logging.info("Connected to broker: " + self.brokerAddr)
    
    '''Disconnect to the remote mqtt broker
    '''    
    def disconnect(self):
        self.client.disconnect()
        logging.info("Disconnected from broker: " + self.brokerAddr)
    
    '''Log the published message
    '''    
    def on_message_pub(self,client, userdata, msg):
        logging.info("\nPublishing:\n" + msg.topic + ": " + msg.payload)
    
    '''Log the received message, and convert it to a SensorData object then convert back by using DataUtil
    '''
    def on_message_come(self,client,userdata,msg):
        logging.info("\nReceived:\n" + msg.topic + ": " + str(msg.payload))
        jsondata = str(msg.payload)#get the received string
        jsondata = jsondata[2:len(jsondata)-1]#delete the character not required 
        #print(jsondata))
        dataUtil = DataUtil.DataUtil()
        sensorData = dataUtil.toSensorDataFromJson(jsondata)
        logging.info("convert json message to SensorData:\n" + str(sensorData))
        jsonstr = dataUtil.toJsonFromSensorData(sensorData)
        logging.info("convert SensorData back to json message:")
        logging.info(jsonstr)
        
    '''Publish the message to a topic and set the qos level
    '''    
    def publishMessage(self, topic, payload, qos):
        self.client.publish(topic, payload, qos)
        self.client.on_message = self.on_message_pub
        self.client.loop_forever()#start loop
        logging.info("Published message to topic " + topic)
    
    '''Subscribe to a topic and set the qos level as 2
    '''
    def subscribeToTopic(self,topic):
        self.client.subscribe(topic, qos=2)
        self.client.on_message = self.on_message_come
        self.client.loop_forever()#start loop
        logging.info("Subscribe to topic " + topic + "successfully")
    
    '''Cancel subscription to a topic
    '''    
    def unSubscribeToTopic(self,topic):
        self.client.unsubscribe(topic)
        logging.info("Unsubscribe to topic " + topic) 
        
   
