from labs.module06 import MqttClientConnector

'''This class is the mqtt client that subscribe to a topic.
'''
class MqttSubClientTestApp():
    
    ''' Connect to the mqtt client, and subscribe to the given topic
    '''
    def run(self):
        mqttClient = MqttClientConnector.MqttClientConnector()
        mqttClient.connect()
        topic = 'mqtt-test'
        mqttClient.subscribeToTopic(topic)
        
'''The main function running in loop
'''        
if __name__ == '__main__':
    mqttsub = MqttSubClientTestApp()
    while True:
        mqttsub.run()