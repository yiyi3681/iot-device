import psutil
import threading
from time import sleep

#create an adaptor class extends threading.Thread
class SystemPerformanceAdaptor(threading.Thread):
    
    #constructor, set default sleep time as 5s
    def __init__(self,second=5 ):
        super(SystemPerformanceAdaptor, self).__init__()
        #if second has value, set the new value 
        if second > 0:
            self.second = second
    #set output information flag
    def setEnableAdaptorFlag(self, flag):
        self.enableAdaptor = flag
    #show information    
    def run(self):
        while True:
            if self.enableAdaptor:
                print('\n--------------------')
                print('New system performance readings:')
                print(' ' + str(psutil.cpu_stats()))
                print(' ' + str(psutil.virtual_memory()))
                #print(' ' + str(psutil.sensors_temperatures(False)))
            sleep(self.second)