from time           import sleep
from labs.module01 import SystemPerformanceAdaptor

sysPerfAdaptor = SystemPerformanceAdaptor.SystemPerformanceAdaptor()


print("Starting system performance app daemon thread...")
#set enableAdaptor as true
sysPerfAdaptor.setEnableAdaptorFlag(True) 
#start the run function in SystemPerformanceAdaptor
sysPerfAdaptor.start() 

#keep running
while (True):
    sleep(5)
    pass