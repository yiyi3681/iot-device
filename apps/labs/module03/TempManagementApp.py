#import os,sys
#sys.path.append('/home/pi/workspace/iot-device/apps')
from time           import sleep
from labs.module03 import TempSensorAdaptor

sysPerfAdaptor = TempSensorAdaptor.TempSensorAdaptor()
print("Starting system performance app daemon thread...")
sysPerfAdaptor.setEnableAdaptorFlag(True)#set enableAdaptor to true
sysPerfAdaptor.start()#start the run function in TempSensorAdaptor

#keep running
while (True):
    sleep(5)
    pass