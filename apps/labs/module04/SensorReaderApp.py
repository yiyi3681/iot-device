import os,sys
sys.path.append('/home/pi/workspace/iot-device/apps')
from time           import sleep
from labs.module04 import I2CSenseHatAdaptor

i2cSenseHat = I2CSenseHatAdaptor.I2CSenseHatAdaptor()#instantiating an I2CSenseHatAdaptor class
print("Starting system performance app daemon thread...")#starting demo

i2cSenseHat.run()#run the thread

while (True):
    sleep(5)
    pass#loop every 5 seconds