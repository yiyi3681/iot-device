#import os,sys
#sys.path.append('/home/pi/workspace/iot-device/apps')
from time           import sleep
from labs.module05 import TempSensorAdaptor

sysPerfAdaptor = TempSensorAdaptor.TempSensorAdaptor()#instantiating a TempSensorAdaptor object
print("Starting system performance app daemon thread...")
sysPerfAdaptor.setEnableAdaptorFlag(True)#set enableAdaptor to true
sysPerfAdaptor.start()#start the run function in TempSensorAdaptor

'''keep running loop'''
while (True):
    sleep(5)
    pass