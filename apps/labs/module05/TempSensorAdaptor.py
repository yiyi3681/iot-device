#import os,sys
#sys.path.append('/home/pi/workspace/iot-device/apps')
import threading
from time import sleep
from random import uniform
import pickle
from labs.common import DataUtil
from labs.common import SensorData
from labs.module02 import SmtpClientConnector
from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.common.ActuatorData import ActuatorData
from labs.module03.TempActuatorEmulator import TempActuatorEmulator

DEFAULT_FATE_IN_SEC = 5

'''This class is the temperature sensor adaptor, it implements the flowing things:
   1. getting emulated temperature values and sending them to SensorData, showing sensor reading information;
   2. when the deviation between current value and average value exceeds the threshold, 
      the SensorData string will be converted to a JSON string through DataUtil which will be written to file and sent by email;
   3. when the deviation between current value and nominal value exceeds the threshold,
      the TempActuator will be triggered to adjust the temperature.
'''
class TempSensorAdaptor(threading.Thread):
    '''setting required parameters' values, instantiating required objects
    '''
    enableAdaptor   = False
    actuatorData = None
    tempActuatorEmulator = None
    connector = SmtpClientConnector.SmtpClientConnector()
    sensorData = SensorData.SensorData() 
    alertDiff = 5
    rateInSec       = DEFAULT_FATE_IN_SEC
    enableEmulator = False
    lowVal = 0
    highVal = 30
    curTemp = 0
    isPrevTempSet = True
    #filedir = 'C:/Users/yiyi3/eclipse-workspace/iot-device/sensordata_json.json'
    
    '''The constructor loads the configuration file and instantiates required objects
    '''
    def __init__(self):
        super(TempSensorAdaptor, self).__init__()    
        self.config = ConfigUtil.ConfigUtil('')
        self.config.loadConfig()
        self.actuatorData = ActuatorData()
        self.tempActuatorEmulator = TempActuatorEmulator()
        self.dataUtil = DataUtil.DataUtil()
    
    '''setting the trigger of run()
    '''     
    def setEnableAdaptorFlag(self, flag):
        self.enableAdaptor = flag
    
    '''This method is the execution of the whole class function
    ''' 
    def run(self):
        nominalTemp = int(self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP_KEY)) #get the value of nominal temperature from ConfigConst class
        while True:
            if self.enableAdaptor:
                self.curTemp = uniform(float(self.lowVal), float(self.highVal))#get a emulated temperature value
                self.sensorData.addValue(self.curTemp) #add current temperature to SensorData class
                print('\n--------------------') 
                print('New sensor readings:') 
                print(' ' + str(self.sensorData))#show sensorData string
                if self.isPrevTempSet == False: #skip the first temperature value
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True 
                else:
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff): #if the deviation between current value and average value exceeds threshold
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggeringalert...')
                        jsondata = self.dataUtil.toJsonFromSensorData(self.sensorData)#converting SensorData object to JSON string through DataUtil.toJsonFromSensorData()
                        print("JSON string data:")
                        print(jsondata)#show JSON string
                        jsonfile = open('C:/Users/yiyi3/eclipse-workspace/iot-device/sensordata_json.json','wb')#creating a json file to write into
                        pickle.dump(jsondata,jsonfile)#writing JSON string to the json file
                        jsonfile.close()#close the json file
                        self.connector.publishMessage('Exceptional sensor data [test]', jsondata)#sending email
                    if (abs(self.curTemp - nominalTemp) > self.alertDiff): #if the deviation between current value and nominal value exceeds the threshold
                        print('\n+++++++++++++++++++') 
                        val = self.curTemp - nominalTemp#computing the deviation value
                        self.actuatorData.setValue(val)#set the deviation value in ActuatorData object
                        '''if current value is more than nominal value, set command as 1, otherwise 0'''
                        if (val > 0):
                            print('val > 0')
                            self.actuatorData.setCommand(1)
                        else:
                            print('val < 0')
                            self.actuatorData.setCommand(0)
                        self.tempActuatorEmulator.processMessage(self.actuatorData)#triggering the TempActuatorEmulator to generate temperature adjustment message showing in senseHat
            sleep(self.rateInSec)    