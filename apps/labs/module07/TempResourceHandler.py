import logging
import time
from coapthon.resources.resource import Resource
from labs.common import DataUtil

'''This class inherits Resource class and overrides methods to process the requests from the client and send responses
'''
class TempResourceHandler(Resource):
    logging.basicConfig(level=logging.INFO)
    
    '''Initiate resource with name
    '''
    def __init__(self,name):
        Resource.__init__(self, name = 'temp')
    
    '''Override render methods of requests
    '''    
    def render_GET_advanced(self, request, response):
        return self, response, self.render_GET_separate

    def render_POST_advanced(self, request, response):
        return self, response, self.render_POST_separate

    def render_PUT_advanced(self, request, response):

        return self, response, self.render_PUT_separate

    def render_DELETE_advanced(self, request, response):
        return self, response, self.render_DELETE_separate

    '''Send response message to GET request
    '''
    def render_GET_separate(self, request, response):
        time.sleep(5)
        response.payload = "received GET request"
        response.max_age = 20
        logging.info("Handling GET: " + response.payload)
        return self, response
    
    '''Send response message to POST request,
       convert the POST message to SensorData and convert back to JSON
    '''
    def render_POST_separate(self, request, response):
        logging.info("POST message: " + request.payload)
        jsondata = str(request.payload)#get the received string
        dataUtil = DataUtil.DataUtil()
        sensorData = dataUtil.toSensorDataFromJson(jsondata)
        logging.info("convert json message to SensorData:\n" + str(sensorData))
        jsonstr = dataUtil.toJsonFromSensorData(sensorData)
        logging.info("convert SensorData back to json message:")
        logging.info(jsonstr)
        response.payload = "received POST request"
        logging.info("Handling POST: " + response.payload)
        return self, response

    '''Send response message to PUT request
    '''
    def render_PUT_separate(self, request, response):
        self.payload = request.payload
        response.payload = "received PUT request"
        logging.info("Handling PUT: " + response.payload)
        return self, response

    '''Send response message to DELETE request
    '''
    def render_DELETE_separate(self, request, response):
        response.payload = "received DELETE request"
        logging.info("Handling DELETE: " + response.payload)
        return True, response
    
    