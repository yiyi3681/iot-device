import logging
from labs.common import SensorData
from labs.common import DataUtil
from labs.module07.CoapClientConnector import CoapClientConnector

'''This class is used to set up a Coap client and send four kinds of request to the server
'''
class CoapClientTestApp:
    resource = 'temp'#define the name of the resource
    logging.basicConfig(level=logging.INFO)

    '''Instantiate a simple SensorData and convert it to a JSON string
    '''
    def generateMessage(self):
        sensorData = SensorData.SensorData()
        dataUtil = DataUtil.DataUtil()
        sensorData.curValue = 20
        sensorData.minValue = 3
        sensorData.maxValue = 28
        sensorData.avgValue = 17
        sensorData.sampleCount = 6
        jsondata = dataUtil.toJsonFromSensorData(sensorData)#converting SensorData object to JSON string through DataUtil.toJsonFromSensorData()
        logging.info("Post JSON string data:")
        logging.info(jsondata)#show JSON string
        return jsondata
    
    '''Instantiate a CoapClientConnector and issue the four request.
       Send the JSON string through POST and PUT.
       Stop the client after requests.
    '''
    def run(self):
        jsonstr = self.generateMessage()
        coapclient = CoapClientConnector()
        coapclient.initClient()
        coapclient.handleGetTest(self.resource)
        coapclient.handlePostTest(self.resource, jsonstr)
        coapclient.handlePutTest(self.resource, jsonstr)
        coapclient.handleDeleteTest(self.resource)
        coapclient.stop()
        
'''Main function
'''
if __name__ == '__main__':
    coapClientApp = CoapClientTestApp()
    coapClientApp.run()