from labs.module07.CoapServerConnector import CoapServerConnector
from labs.common import ConfigUtil 
from labs.common import ConfigConst

'''This class instantiates a CoapServerConnector object and calls the method to start the server
'''
class CoapServerTestApp:
    '''Load the configuration file and set the host and port
    '''
    config = ConfigUtil.ConfigUtil('')
    config.loadConfig()
    host = config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.HOST_KEY)
    port = int(config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.PORT_KEY))
    
    '''Start the server
    '''
    def run(self):
        coapServer = CoapServerConnector(self.host,self.port)
        coapServer.start()
        
'''Main function
'''
if __name__ == '__main__':
    coapServerApp = CoapServerTestApp()
    coapServerApp.run()