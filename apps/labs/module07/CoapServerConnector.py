import logging
from coapthon.server.coap import CoAP
from labs.common import ConfigUtil 
from labs.common import ConfigConst
from labs.module07 import TempResourceHandler

'''This class contains the main method to build a Coap server, 
   including initiates a CoAP object and adds resource to it
'''
class CoapServerConnector(CoAP):
    '''Load the configuration file and set the host and port,
       define the path of the resource
    '''
    config = ConfigUtil.ConfigUtil('')
    config.loadConfig()
    host = config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.HOST_KEY)
    port = int(config.getProperty(ConfigConst.COAP_DEVICE_SECTION, ConfigConst.PORT_KEY))
    name = 'temp'
    path = 'temp'
    logging.basicConfig(level=logging.INFO)
    
    '''Constructor, initiate a CoAP object with host and port
    '''
    def __init__(self,host,port):
        CoAP.__init__(self, (host, port))
        logging.info('\tHost: ' + host) 
        logging.info('\tPort: ' + str(port))
    
    '''Instantiate a CoapServerConnector and a TempResourceHandler,
       add the TempResourceHandler to the CoapServerConnector
    '''        
    def initServer(self):
        server = CoapServerConnector(self.host, self.port)
        tempResource = TempResourceHandler.TempResourceHandler(self.name)
        server.add_resource(self.path, tempResource)
    
    '''Add the path and resource to CoAP by add_resource method
    '''
    def add_resource(self, path, resource):
        return CoAP.add_resource(self, path, resource)
    
    '''Start the server
    '''
    def start(self):
        self.initServer()
        try:
            self.listen(10)
        except KeyboardInterrupt:
            logging.info("Server Shutdown")
            self.close()