import logging
from coapthon.client.helperclient import HelperClient
from labs.common import ConfigUtil 
from labs.common import ConfigConst

'''This class realizes the main function of a Coap Client. It initiates a HelperClient and sets up the host and port, 
   as well as defines the way to process the four kinds of request message and their response.
'''
class CoapClientConnector(object):
    '''Load the configuration file and set the host and port
    '''
    config = ConfigUtil.ConfigUtil('')
    config.loadConfig()
    host = config.getProperty(ConfigConst.COAP_DEVICE_SECTION,ConfigConst.HOST_KEY)
    port = int(config.getProperty(ConfigConst.COAP_DEVICE_SECTION,ConfigConst.PORT_KEY))
    logging.basicConfig(level=logging.INFO)
    
    '''Constructor 
    '''   
    def __init__(self):
        logging.info('\tHost: ' + self.host) 
        logging.info('\tPort: ' + str(self.port))
           
    '''Instantiate a HelperClient with host and port
    '''
    def initClient(self): 
        try:
            self.url = "coap://" + self.host + ":" + str(self.port)
            self.client = HelperClient(server=(self.host,self.port))
            logging.info("Created CoAP client ref: " + str(self.client))
            logging.info("url: " + self.url) 
        except Exception:
            logging.info("Failed to create CoAP helper client reference using host: " + self.host) 
            pass
    
    '''Log out the response of GET request
    '''
    def handleGetTest(self, resource):
        response = self.client.get(resource)
        logging.info('GET response from server: ' + response.pretty_print())
       
    '''POST the given message to the server and log out the response
    '''
    def handlePostTest(self, resource, payload):
        response = self.client.post(resource, payload)
        logging.info('POST response from server: ' + response.pretty_print())
    
    '''PUT the given message to the server and log out the response
    '''
    def handlePutTest(self, resource, payload):
        response = self.client.put(resource, payload)
        logging.info('PUT response from server: ' + response.pretty_print())
    
    '''Log out the response of DELETE request
    '''
    def handleDeleteTest(self, resource):
        response = self.client.delete(resource)
        logging.info('DELETE response from server: ' + response.pretty_print())
    
    '''Stop the client
    '''
    def stop(self):
        self.client.stop()