from labs.final_gateway import MqttClientConnector
from time import sleep

'''This class is the mqtt client that subscribe to a topic.
'''
class MqttSubClientTestApp():
    
    ''' Connect to the mqtt client, and subscribe to the given topic
    '''
    def run(self):
        mqttClient = MqttClientConnector.MqttClientConnector()
        mqttClient.connect()
        topic = 'sensor'
        mqttClient.subscribeToTopic(topic)
        
'''The main function running in loop
'''        
if __name__ == '__main__':
    mqttsub = MqttSubClientTestApp()
    mqttsub.run()