from labs.final_gateway import MqttClientConnector
from labs.final_gateway import CloudConnector

import logging,sys
from time import sleep
import json

'''This class is the mqtt client that publishes messages to a topic.
   It gets actuator values from cloud and send them to SensorData that can be converted to a JSON string,
   and publish the string by using MqttClientConnector every minute.
'''
class MqttPubClientTestApp():
    logging.basicConfig(level=logging.INFO)#set the log to display level info

    '''Connect to the mqtt broker, and publish a test message to the given topic
    '''
    def run(self):
        while(True):
            mqttClient = MqttClientConnector.MqttClientConnector()
            mqttClient.connect()
            topic = 'actuator'
            cloud = CloudConnector.CloudConnector()
            payload = cloud.getNewValue()
            payload = json.dumps(payload)
            qos = 2
            mqttClient.publishMessage(topic, str(payload), qos)
            sleep(60)
        
'''The main function
'''        
if __name__ == '__main__':
    mqttpub = MqttPubClientTestApp()
    mqttpub.run()

        
    
    
