from ubidots import ApiClient

'''This class is a bridge to access cloud variables on Ubidots.
   It can send and receive the values of variables by calling Ubidots API
'''
class CloudConnector:
    '''Instantiate the API client by setting the token and base url
    '''
    api = ApiClient(token='A1E-De800DyEMB65xtofvnu8GRvgUyv3hb', base_url='https://things.ubidots.com/api/v1.6/')
    
    '''Get the temperature value which should be saved in the cloud next time
    '''
    def setTemperature(self,data):
        self.temperature = data
    
    '''Get the huimdity value which should be saved in the cloud next time
    '''    
    def setHumidity(self,data):
        self.humidity = data
    
    '''Using PUT request to update the temperature and humidity values formatted in dictionary 
    '''    
    def putNewValue(self):
        temp = self.api.get_variable('5cb8e8f5c03f972c637ac750')
        humd = self.api.get_variable('5cb8e90cc03f972c80fcdb79')
        tempdict = {'value' : self.temperature}
        humddict = {'value' : self.humidity}
        temp.save_value(tempdict)
        humd.save_value(humddict)
    
    '''GET the latest values of temperature actuator and humidity actuator
    '''    
    def getNewValue(self):
        tempActuator = self.api.get_variable('5cb8f7a4c03f9737fe780465')
        humdActuator = self.api.get_variable('5cb9145cc03f974ec0ad97ca')
        tempCommand = tempActuator.get_values()
        humdCommand = humdActuator.get_values()
        message = {'TempActuator' : tempCommand[0]['value'], 'HumidityActuator' : humdCommand[0]['value']}
        return message
        

