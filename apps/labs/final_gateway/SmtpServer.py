from labs.final_gateway import SmtpClientConnector
from labs.final_gateway import CloudConnector

'''This class accepts the actuator data and decides whether need to an email to the user
'''
class SmtpServer:
    '''Accepts the data sent by cloud
    '''
    def addMessage(self,jsondata):
        self.message = jsondata
    
    '''If the actuator value is 1, publish an email containing the command string
    '''    
    def publish(self):
        cloud = CloudConnector.CloudConnector()
        smtp = SmtpClientConnector.SmtpClientConnector()
        command = cloud.getNewValue()
        if command['TempActuator'] == 1:
            smtp.publishMessage('temperature', self.message + "\n" + "Open the cool function")
        if command['HumidityActuator'] == 1:
            smtp.publishMessage('Humidity', self.message + "\n" + "Open the water function")
            
            
            
