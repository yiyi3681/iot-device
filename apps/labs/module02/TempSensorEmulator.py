
import threading
from time import sleep
from random import uniform
from labs.common import SensorData
from labs.module02 import SmtpClientConnector

DEFAULT_FATE_IN_SEC = 5
class TempSensorEmulator(threading.Thread):
    rateInSec       = DEFAULT_FATE_IN_SEC
    connector = SmtpClientConnector.SmtpClientConnector()
    sensorData = SensorData.SensorData() 
    alertDiff = 5
    enableEmulator = False
    lowVal = 0
    highVal = 30
    curTemp = 0
    isPrevTempSet = True
    
    #Constructor
    def __init__(self):
        super(TempSensorEmulator, self).__init__()
    
    #set enableEmulator    
    def setEnableEmulatorFlag(self, flag):
        self.enableEmulator = flag
        
    def run(self):
        while True:
            if self.enableEmulator:
                #generate temperature value between 0 C and 30 C
                self.curTemp = uniform(float(self.lowVal), float(self.highVal)) 
                #add current temperature value
                self.sensorData.addValue(self.curTemp) 
                print('\n-------------------+-') 
                print('New sensor readings:') 
                print(' ' + str(self.sensorData))
                #skip if this is the first temperature
                if self.isPrevTempSet == False: 
                    self.prevTemp = self.curTemp 
                    self.isPrevTempSet = True 
                #compute the loss between current value and average value
                elif (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                    print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggeringalert...')
                    #publish the email message
                    self.connector.publishMessage('Exceptional sensor data [test]', str(self.sensorData))
            sleep(self.rateInSec)    