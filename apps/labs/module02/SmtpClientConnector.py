
import os,sys
sys.path.append('C:/Users/yiyi3/eclipse-workspace/iot-device/apps')
from email.mime.multipart import MIMEMultipart
from labs.common import ConfigUtil
from labs.common import ConfigConst
from email.mime.text import MIMEText
import smtplib

class SmtpClientConnector(object):
   
    #Constructor, instantiate ConfigUtil() class to load the config file
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('')
        self.config.loadConfig()
        print('Configuration data...\n' + str(self.config))
        
    def publishMessage(self, topic, data):
        #set the default value
        host    = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY)
        port    = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY)
        fromAddr    = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY)
        authToken   = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY)
        nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP_KEY)
        
        #instantiate a MIMEMultipart() class to send email with attached files
        msg = MIMEMultipart()
        msg['From'] = fromAddr
        msg['To'] = toAddr 
        msg['Subject'] = topic 
        #add data as text attached file
        msgBody = str(data)
        msg.attach(MIMEText(msgBody))
        #Return the entire message flattened as a string 
        msgText = msg.as_string()
        
        # send SSL safe e-mail 
        smtpServer = smtplib.SMTP_SSL(host, port) 
        #enable user authorization
        smtpServer.ehlo() 
        #log in the account and send email
        smtpServer.login(fromAddr, authToken) 
        smtpServer.sendmail(fromAddr, toAddr, msgText) 
        smtpServer.close()