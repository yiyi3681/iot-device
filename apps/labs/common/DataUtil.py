import json
from labs.common import SensorData
from labs.common import ActuatorData

'''This class realizes functions of the conversion between two kinds of object(SensorData, ActuatorData) and JSON string
'''
class DataUtil():
   
    '''This method uses a SensorData object as parameter, and loads its values to a new dictionary jsondata.
       Then using json.dumps() to encode jsondata to a JSON string which is the returned parameter.
    '''
    def toJsonFromSensorData(self,SensorData):
        jsondata = {}#creating new dictionary as a container
        jsondata['name'] = SensorData.name
        jsondata['timeStamp'] = SensorData.timeStamp
        jsondata['curValue'] = SensorData.curValue
        jsondata['avgValue'] = SensorData.avgValue
        jsondata['sampleCount'] = SensorData.sampleCount
        jsondata['minValue'] = SensorData.minValue
        jsondata['maxValue'] = SensorData.maxValue
        print("converting SensorData object to JSON string...")
        jsonstr = json.dumps(jsondata)
        '''
        with open(filedir, 'w') as outfile:
            json.dump(jsondata, outfile)#converting to JSON
        #print(jsondata)
        '''
        return jsonstr
    
    '''This method gets a JSON string as a parameter and decodes it to a python dictionary jsondata through json.loads().
       It instantiates a SensorData object which accepts values from jsondata and then be returned.
    '''
    def toSensorDataFromJson(self,jsondata):
        jsondata = json.loads(jsondata)#decoding JSON 
        sensorData = SensorData.SensorData()#instantiating new SensorData object
        print("converting JSON string to SensorData object...")
        sensorData.name = jsondata['name']
        sensorData.timeStamp = jsondata['timeStamp']
        sensorData.curValue = jsondata['curValue']
        sensorData.avgValue = jsondata['avgValue']
        sensorData.sampleCount = jsondata['sampleCount']
        sensorData.minValue = jsondata['minValue']
        sensorData.maxValue = jsondata['maxValue']
        #print(self.sensorData)
        return sensorData
    
    '''This method uses an ActuatorData object as parameter, and loads its values to a new dictionary jsondata.
       Then using json.dumps() to encode jsondata to a JSON string which is the returned parameter.
    '''
    def toJsonFromActuatorData(self,ActuatorData):
        jsondata = {}#creating new dictionary as a container
        jsondata['name'] = ActuatorData.name
        jsondata['timeStamp'] = ActuatorData.timestamp
        jsondata['command'] = ActuatorData.command
        jsondata['hasError'] = ActuatorData.hasError
        jsondata['statusCode'] = ActuatorData.statusCode
        jsondata['errCode'] = ActuatorData.errCode
        jsondata['stateData'] = ActuatorData.stateData
        jsondata['curValue'] = ActuatorData.curValue
        print("converting ActuatorData object to JSON string...")
        jsondata = json.dumps(jsondata)#converting to JSON
        #print(jsondata)
        return jsondata
    
    '''This method gets a JSON string as a parameter and decodes it to a python dictionary jsondata through json.loads().
       It instantiates an ActuatorData object which accepts values from jsondata and then be returned.
    '''
    def toActuatorDataFromJson(self,jsondata):
        jsondata = json.loads(jsondata)#decoding JSON
        actuatorData = ActuatorData.ActuatorData()#instantiating new ActuatorData object
        print("converting JSON string to ActuatorData object...")
        actuatorData.name = jsondata['name']
        actuatorData.timeStamp = jsondata['timeStamp']
        actuatorData.command = jsondata['command']
        actuatorData.hasError = jsondata['hasError']
        actuatorData.statusCode = jsondata['statusCode']
        actuatorData.errCode = jsondata['errCode']
        actuatorData.stateData = jsondata['stateData']
        actuatorData.curValue = jsondata['curValue']
        #print(self.actuatorData)
        return actuatorData
        
       