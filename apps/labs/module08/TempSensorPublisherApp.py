from random import uniform
from time import sleep
import logging
from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.module08 import MqttClientConnector

'''This class is a mqtt server with function that publish to TempSenosr topic every 5 seconds
'''
class TempSensorPublisherApp:
    '''Load the configuration file and set the host, token, CA file and topic
    '''
    config = ConfigUtil.ConfigUtil('')
    config.loadConfig()
    host = config.getProperty(ConfigConst.UBIDOTS_CLOUD_SECTION, ConfigConst.HOST_KEY)
    token = config.getProperty(ConfigConst.UBIDOTS_CLOUD_SECTION, ConfigConst.USER_NAME_TOKEN_KEY)
    pem_path = 'C:/Users/yiyi3/eclipse-workspace/ubidots_cert.pem'
    topic = '/v1.6/devices/connected_devices'
    logging.basicConfig(level=logging.INFO)

    '''Instantiate a MqttClientConnector object,
       generate a random temperature value between 0 and 30 and encapsulate it as a dict for converting to JSON
    '''
    def run(self):
        while(True):#loop publishing
            mqttClient = MqttClientConnector.MqttClientConnector(self.host, self.token, self.pem_path)
            mqttClient.connect()
            qos = 2
            tempvalue = uniform(0,30)
            payload = {'tempsensor' : tempvalue}
            logging.info('Published message: ')
            logging.info(payload)
            mqttClient.publishMessage(self.topic, payload, qos)
            sleep(10)
        
'''Main function
'''
if __name__ == '__main__':
    sensorApp = TempSensorPublisherApp()
    sensorApp.run()
