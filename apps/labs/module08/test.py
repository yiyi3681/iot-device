import requests
import random
import time

TOKEN = "A1E-De800DyEMB65xtofvnu8GRvgUyv3hb" # Assign your Ubidots Token
DEVICE = "connected_devices" # Assign the device label to obtain the variable
VARIABLE = "tempactuator" # Assign the variable label to obtain the variable value
DELAY = 5  # Delay in seconds

def get_var(device, variable):
    try:
        url = "https://things.ubidots.com/"
        url = url + \
            "api/v1.6/devices/{0}/{1}/".format(device, variable)
        headers = {"X-Auth-Token": TOKEN, "Content-Type": "application/json"}
        req = requests.get(url=url, headers=headers)
        return req.json()
    except:
        pass


if __name__ == "__main__":
    print(get_var(DEVICE, VARIABLE))