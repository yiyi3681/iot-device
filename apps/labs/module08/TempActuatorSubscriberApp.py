from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.module08 import MqttClientConnector

'''This class is a mqtt client which subscribes to TempActuator by running MqttClientConnector subscribe method 
'''
class TempActuatorSubscriberApp:
    '''Load the configuration file and set the host, token, CA file and topic
    '''
    config = ConfigUtil.ConfigUtil('')
    config.loadConfig()
    host = config.getProperty(ConfigConst.UBIDOTS_CLOUD_SECTION, ConfigConst.HOST_KEY)
    token = config.getProperty(ConfigConst.UBIDOTS_CLOUD_SECTION, ConfigConst.USER_NAME_TOKEN_KEY)
    pem_path = 'C:/Users/yiyi3/eclipse-workspace/ubidots_cert.pem'
    topic = '/v1.6/devices/connected_devices/tempactuator/lv'#lv means only getting the latest value
    
    '''Instantiate a MqttClientConnector and subscribe to TempActuator
    '''
    def run(self):
        mqttClient = MqttClientConnector.MqttClientConnector(self.host, self.token, self.pem_path)
        mqttClient.connect()
        mqttClient.subscribeToTopic(self.topic)
        

'''Main function
'''
if __name__ == '__main__':
    actuatorApp = TempActuatorSubscriberApp()
    actuatorApp.run()
        
    