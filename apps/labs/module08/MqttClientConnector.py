import paho.mqtt.client as mqtt
import time
import logging
import ssl
import json
from sense_hat import SenseHat

'''This class implements MqttCallback working as a connector to realize mqtt protocol.
   It contains the methods which can enable TLS certification, connect and disconnect to a remote host, 
   publish and subscribe messages.
'''
class MqttClientConnector():
    default_protocol = "mqtt"
    mqtt_password = ''
    logging.basicConfig(level=logging.INFO)#set the log to display level info
    
    '''The constructor sets the value of host, port, client id, token, password and ssl certification file
    '''
    def __init__(self, host, token, pem_path):
        self.host = host
        self.port = 8883
        self.client_id = time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))#get the current time as client id
        self.client = mqtt.Client(self.client_id)
        logging.info("Using client ID for broker conn: " + self.client_id)
        self.brokerAddr = self.default_protocol + "://" + host + ":" + str(self.port)
        logging.info("Using URL for broker conn: " + self.brokerAddr)
        self.client.username_pw_set(token, password=self.mqtt_password)#set username and password
        self.client.tls_set(ca_certs=pem_path, certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
        self.client.tls_insecure_set(False)
    
    '''Connect to the remote mqtt broker
    '''    
    def connect(self):
        self.client.connect(self.host, self.port, 600)
        logging.info("Connected to broker: " + self.brokerAddr)
    
    '''Disconnect to the remote mqtt broker
    '''    
    def disconnect(self):
        self.client.disconnect()
        logging.info("Disconnected from broker: " + self.brokerAddr)
    
    '''Log the published message
    '''    
    def on_message_pub(self,client, userdata, msg):
        logging.info("\nPublishing:\n" + msg.topic + ": " + str(msg.payload))
    
    '''Log the received message, and show it on sensehat LED
    '''
    def on_message_come(self,client,userdata,msg):
        logging.info("\nReceived:\n" + msg.topic + ": " + str(msg.payload))
        sh = SenseHat()
        sh.show_message(str(msg.payload))#display the message on sensehat
        
    '''Publish the JSON message to a topic and set the qos level
    '''    
    def publishMessage(self, topic, payload, qos):
        message = json.dumps(payload)#convert dict to JSON
        self.client.on_message = self.on_message_pub
        self.client.publish(topic, message, qos)
        self.client.loop_start()#start loop
        logging.info("Published message to topic " + topic)
    
    '''Subscribe to a topic and set the qos level as 2
    '''
    def subscribeToTopic(self,topic):
        self.client.subscribe(topic, qos=2)
        self.client.on_message = self.on_message_come
        self.client.loop_forever()#start loop
        logging.info("Subscribe to topic " + topic + "successfully")
    
    '''Cancel subscription to a topic
    '''    
    def unSubscribeToTopic(self,topic):
        self.client.unsubscribe(topic)
        logging.info("Unsubscribe to topic " + topic) 
        
   
