import sys
sys.path.append('/home/pi/workspace/iot-device/apps')
from sense_hat import SenseHat

'''This class controls the actuator, it accepts TempActuator and HumdActuator to decide what to do.
   If the corresponding actuator value is 1 which means the actuator is activated, a command string will be displayed on the sense hat.
'''
class Actuator:
    '''Instantiate SenseHat
    '''
    sense = SenseHat()
    
    '''Set the temperature actuator value
    '''
    def setTempCommand(self,command):
        self.tempCommand = command
    
    '''Set the humidity actuator value
    '''    
    def setHumdCommand(self,command):
        self.humdCommand = command
     
    '''If the value is 1, display the string in red
    '''   
    def LEDShow(self):
        red = (255,0,0)
        if self.tempCommand == 1:
            self.sense.show_message("Switch to Cool", text_colour=red)
            
        if self.humdCommand == 1:
            self.sense.show_message("Add Water", text_colour=red)