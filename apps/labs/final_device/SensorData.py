import os,sys
sys.path.append('/home/pi/workspace/iot-device/apps/labs')
from datetime import datetime

'''This class is used to store all the temperature and humidity values and the min, max, average, count, total values
'''
class SensorData:
    timeStamp = None
    name = ['Temperature', 'Humidity']
    curTemp = 0
    avgTemp = 0
    minTemp = 0
    maxTemp = 0
    sampleCount = 0
    curHumd = 0
    avgHumd = 0
    minHumd = 0
    maxHumd = 0
    totTemp = 0
    totHumd = 0
    
    '''Constructor initializes the time stamp
    '''
    def __init__(self):
        self.timeStamp = str(datetime.now())
    
    '''reset new value, max value, min value and average value 
    '''
    def addTemp(self, newTemp):
        self.sampleCount += 1
        if (self.sampleCount == 1):
            self.minTemp = newTemp
        self.timeStamp = str(datetime.now())
        self.totTemp += newTemp
        self.curTemp = newTemp
        
        if (self.curTemp < self.minTemp):
            self.minTemp = self.curTemp
        if (self.curTemp > self.maxTemp):
            self.maxTemp = self.curTemp
        if (self.totTemp != 0 and self.sampleCount > 0):
            self.avgTemp = self.totTemp / self.sampleCount
    
        '''reset new value, max value, min value and average value 
    '''
    def addHumd(self, newHumd):
        if (self.sampleCount == 1):
            self.minHumd = newHumd
        self.totHumd += newHumd
        self.curHumd = newHumd
        
        if (self.curHumd < self.minHumd):
            self.minHumd = self.curHumd
        if (self.curHumd > self.maxHumd):
            self.maxHumd = self.curHumd
        if (self.totHumd != 0 and self.sampleCount > 0):
            self.avgHumd = self.totHumd / self.sampleCount
            
        
    '''override _str_ to print temperature and humidity information
    '''
    def __str__(self):
        customStr = \
            str(self.name[0] + ':'        + \
            os.linesep + '\tTime: '    + self.timeStamp + \
            os.linesep + '\tCurrent: ' + str(self.curTemp) + \
            os.linesep + '\tAverage: ' + str(self.avgTemp) + \
            os.linesep + '\tSamples: ' + str(self.sampleCount) + \
            os.linesep + '\tMin: '     + str(self.minTemp) + \
            os.linesep + '\tMax: '     + str(self.maxTemp)) + \
            str(self.name[1] + ':'        + \
            os.linesep + '\tTime: '    + self.timeStamp + \
            os.linesep + '\tCurrent: ' + str(self.curHumd) + \
            os.linesep + '\tAverage: ' + str(self.avgHumd) + \
            os.linesep + '\tSamples: ' + str(self.sampleCount) + \
            os.linesep + '\tMin: '     + str(self.minHumd) + \
            os.linesep + '\tMax: '     + str(self.maxHumd))
        return customStr
