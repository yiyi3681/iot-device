import json,sys
sys.path.append('/home/pi/workspace/iot-device/apps')
from labs.final_device import SensorData

'''This class realizes functions of the conversion between SensorData and JSON string
'''
class DataUtil():
   
    '''This method uses a SensorData object as parameter, and loads its values to a new dictionary jsondata.
       Then using json.dumps() to encode jsondata to a JSON string which is the returned parameter.
    '''
    def toJsonFromSensorData(self,SensorData):
        jsondata = {}#creating new dictionary as a container
        jsondata['name'] = SensorData.name
        jsondata['timeStamp'] = SensorData.timeStamp
        jsondata['curTemp'] = SensorData.curTemp
        jsondata['avgTemp'] = SensorData.avgTemp
        jsondata['sampleCount'] = SensorData.sampleCount
        jsondata['minTemp'] = SensorData.minTemp
        jsondata['maxTemp'] = SensorData.maxTemp
        jsondata['curHumd'] = SensorData.curHumd
        jsondata['avgHumd'] = SensorData.avgHumd
        jsondata['minHumd'] = SensorData.minHumd
        jsondata['maxHumd'] = SensorData.maxHumd
        print("converting SensorData object to JSON string...")
        jsonstr = json.dumps(jsondata)
        '''
        with open(filedir, 'w') as outfile:
            json.dump(jsondata, outfile)#converting to JSON
        #print(jsondata)
        '''
        return jsonstr
    
    '''This method gets a JSON string as a parameter and decodes it to a python dictionary jsondata through json.loads().
       It instantiates a SensorData object which accepts values from jsondata and then be returned.
    '''
    def toSensorDataFromJson(self,jsondata):
        jsondata = json.loads(jsondata)#decoding JSON 
        sensorData = SensorData.SensorData()#instantiating new SensorData object
        print("converting JSON string to SensorData object...")
        sensorData.name = jsondata['name']
        sensorData.timeStamp = jsondata['timeStamp']
        sensorData.curTemp = jsondata['curTemp']
        sensorData.avgTemp = jsondata['avgTemp']
        sensorData.sampleCount = jsondata['sampleCount']
        sensorData.minTemp = jsondata['minTemp']
        sensorData.maxTemp = jsondata['maxTemp']
        sensorData.curHumd = jsondata['curHumd']
        sensorData.avgHumd = jsondata['avgHumd']
        sensorData.minHumd = jsondata['minHumd']
        sensorData.maxHumd = jsondata['maxHumd']
        #print(self.sensorData)
        return sensorData