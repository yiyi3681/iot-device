import sys
sys.path.append('/home/pi/workspace/iot-device/apps/labs')
from sense_hat import SenseHat

'''This class acquires the values of temperature and humidity from sense hat
'''
class SensorReader:
    sense = SenseHat()
    
    '''read temperature
    '''
    def getTemp(self):
        self.sense.clear()
        temp = self.sense.get_temperature()
        return temp
    
    '''read humidity
    '''
    def getHumd(self):
        self.sense.clear()
        humd = self.sense.get_humidity()
        return humd