import sys
sys.path.append('/home/pi/workspace/iot-device/apps')
from labs.final_device import MqttClientConnector
from labs.final_device import SensorData
from labs.final_device import DataUtil
from labs.final_device import SensorReader
import logging
from time import sleep


'''This class is the mqtt client that publishes messages to a topic.
   It gets temperature and humidity values from sensor and sends them to SensorData which can be converted to a JSON string,
   and publishes the string by calling MqttClientConnector every minute.
'''
class MqttPubClientTestApp():
    logging.basicConfig(level=logging.INFO)#set the log to display level info

    '''Connect to the mqtt broker, and publish a test message to the given topic
    '''
    def run(self):
        mqttClient = MqttClientConnector.MqttClientConnector()
        mqttClient.connect()
        sr = SensorReader.SensorReader()
        sd = SensorData.SensorData()
        dataUtil = DataUtil.DataUtil()
        topic = 'sensor'#this topic is used to deliver sensor data
        while(True):
            temp = sr.getTemp()#read temperature value from sensor
            humd = sr.getHumd()#read humidity value from sensor
            sd.addTemp(temp)#pass the value to SensorData
            sd.addHumd(humd)#pass the value to SensorData
            jsondata = dataUtil.toJsonFromSensorData(sd)#convert to JSON
            qos = 2
            print(jsondata)
            mqttClient.publishMessage(topic, str(jsondata), qos)
            sleep(60)
        
'''The main function
'''        
if __name__ == '__main__':
    mqttpub = MqttPubClientTestApp()
    mqttpub.run()

        
    
    
